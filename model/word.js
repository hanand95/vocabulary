const mongoose = require('mongoose')
const Schema = mongoose.Schema

const WordSchema = new Schema(
    {
        word: {
            type: String,
            required: [true, 'Word required!']
        },
        definition: {
            type: String,
            required: [true, 'Definition required!']
        }
    }
)

const Word = mongoose.model('word', WordSchema, 'word')

module.exports = Word