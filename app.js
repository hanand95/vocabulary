const express = require ('express')
const app = express()
const port = 3000

const mongoose = require('mongoose')
const url = "mongodb+srv://drake:114422@cluster0.ftkfj.mongodb.net/vocabulary?retryWrites=true&w=majority"
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })

app.use(express.json())
app.use('/api/word', require('./route/route'))

app.listen(port, () => {
    console.log('server\'s listening on port' + port)
})