const express = require('express')
const router = express.Router()
const Word = require('../model/word')

router.use(express.urlencoded({ extended: true }));
router.use(express.json());

router.get('/', (req, res, next) => {
    Word.find({}).then(words => {
        console.log(words)
        res.send(words)
    })
})
router.get('/:word', (req, res, next) => {
    Word.find({word: req.params.word.toString()}).then(word => {
        console.log(word)
        res.send(word)
    }).catch(next)
})
router.post('/', (req, res, next) => {
    Word.create(req.body).then(word => {
        console.log(word)
        res.send(word)
    })
})

module.exports = router